package com.example.moncvapp;

public class Formation {
    private String dateObtention, nomDeDiplome,lieu ;

    public Formation(String dateObtention, String nomDeDiplome, String lieu) {
        this.dateObtention = dateObtention;
        this.nomDeDiplome = nomDeDiplome;
        this.lieu = lieu;
    }

    public String getDateObtention() {
        return dateObtention;
    }

    public void setDateObtention(String dateObtention) {
        this.dateObtention = dateObtention;
    }

    public String getNomDeDiplome() {
        return nomDeDiplome;
    }

    public void setNomDeDiplome(String nomDeDiplome) {
        this.nomDeDiplome = nomDeDiplome;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }
}
