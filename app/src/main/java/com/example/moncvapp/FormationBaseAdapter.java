package com.example.moncvapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class FormationBaseAdapter extends BaseAdapter {
    private ArrayList<Formation> formations;
    private Context context;

    public FormationBaseAdapter(ArrayList<Formation> formations, Context context) {
        this.formations = formations;
        this.context = context;
    }

    @Override
    public int getCount() {
        return formations.size();
    }

    @Override
    public Object getItem(int i) {
        return formations.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null)
        {
            view = LayoutInflater.from(context).inflate(R.layout.item_formation, viewGroup, false);
        }

        TextView textViewDateObtention = view.findViewById(R.id.textViewDateObtention);
        textViewDateObtention.setText(formations.get(i).getDateObtention());

        TextView textViewNomDiplome = view.findViewById(R.id.textViewNomFormation);
        textViewNomDiplome.setText(formations.get(i).getNomDeDiplome());

        TextView textViewLieu = view.findViewById(R.id.textViewLieu);
        textViewLieu.setText(formations.get(i).getLieu());

        return view;
    }
}
