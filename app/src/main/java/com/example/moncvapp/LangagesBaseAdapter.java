package com.example.moncvapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

public class LangagesBaseAdapter extends BaseAdapter {
    private ArrayList<Langages> langages;
    private Context context;

    public LangagesBaseAdapter(ArrayList<Langages> langages, Context context) {
        this.langages = langages;
        this.context = context;
    }

    @Override
    public int getCount() {
        return langages.size();
    }

    @Override
    public Object getItem(int i) {
        return langages.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null)
        {
            view = LayoutInflater.from(context).inflate(R.layout.item_langage, viewGroup, false);
        }

        TextView textViewDateLangages = view.findViewById(R.id.textViewNomLanagage);
        textViewDateLangages.setText(langages.get(i).getNom());

        RatingBar textViewDateRatingBar = view.findViewById(R.id.ratingBar);
        textViewDateRatingBar.setRating(langages.get(i).getLevel());

        return view;
    }


}
