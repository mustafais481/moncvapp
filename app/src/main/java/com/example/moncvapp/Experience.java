package com.example.moncvapp;

public class Experience {
    private String date_debut;
    private String date_fin;
    private String entreprise;
    private String description;

    public Experience(String date_debut, String date_fin, String entreprise, String description)
    {
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.entreprise = entreprise;
        this.description = description;
    }

    public String getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(String date_debut) {
        this.date_debut = date_debut;
    }

    public String getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(String date_fin) {
        this.date_fin = date_fin;
    }

    public String getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(String entreprise) {
        this.entreprise = entreprise;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
