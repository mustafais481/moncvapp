package com.example.moncvapp;

import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class CompetenceActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_competences);

        ArrayList<Langages> langages = new ArrayList<>();

        langages.add(new Langages("C#", 4));
        langages.add(new Langages("Java", 3));
        langages.add(new Langages("PHP", 3));

        ListView listViewLangages = findViewById(R.id.listLangages);
        LangagesBaseAdapter adapter = new LangagesBaseAdapter(langages, this);
        listViewLangages.setAdapter(adapter);

        ArrayList<Project> projects = new ArrayList<>();

        projects.add(new Project("Application Android 1", "Application Spotify", "https://github.com/"));
        projects.add(new Project("Application Android 2", "Application E-commerce", "https://github.com/"));
        projects.add(new Project("Application Android 3", "Application Reminder", "https://github.com/"));

        ListView listViewProjects = findViewById(R.id.listProjets);
        ProjectsBaseAdapter adapterProject = new ProjectsBaseAdapter(projects, this);
        listViewProjects.setAdapter(adapterProject);
    }
}
