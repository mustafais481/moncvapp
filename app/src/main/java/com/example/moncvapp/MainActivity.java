package com.example.moncvapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<Experience> experiences = new ArrayList<>();

        experiences.add(new Experience("12-04-2021", "20-04-2023", "AXA", "Backend developer and cloud"));
        experiences.add(new Experience("01-01-2020", "20-2-2021", "AXA", "Stagaire en Cloud"));

        ListView listViewExperience = findViewById(R.id.listExperience);
        ExperienceBaseAdapter adapter = new ExperienceBaseAdapter(experiences, this);
        listViewExperience.setAdapter(adapter);

        ArrayList<Formation> formations = new ArrayList<>();

        formations.add(new Formation("01-01-2023", "Concepteur développeur d'applications", "CFA-INSTA"));
        formations.add(new Formation("01-01-2022", "BTS-SIO", "CFA-INSTA"));
        formations.add(new Formation("01-01-2020", "Année préparatoire", "Sorbonne université"));

        ListView listViewFormation = findViewById(R.id.listFormations);
        FormationBaseAdapter adapterFormation = new FormationBaseAdapter(formations, this);
        listViewFormation.setAdapter(adapterFormation);

        Button button = findViewById(R.id.buttonComptetences);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this,CompetenceActivity.class);
        startActivity(intent);
    }
}