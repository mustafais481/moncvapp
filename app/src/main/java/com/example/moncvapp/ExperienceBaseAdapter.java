package com.example.moncvapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ExperienceBaseAdapter extends BaseAdapter {
    private ArrayList<Experience> experiences;
    private Context context;

    public ExperienceBaseAdapter(ArrayList<Experience> experiences, Context context) {
        this.experiences = experiences;
        this.context = context;
    }

    @Override
    public int getCount() {
        return experiences.size();
    }

    @Override
    public Object getItem(int i) {
        return experiences.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        if(view == null)
        {
            view = LayoutInflater.from(context).inflate(R.layout.item_experience, viewGroup, false);
        }

        TextView textViewDateDebut = view.findViewById(R.id.textViewDateDebut);
        textViewDateDebut.setText(experiences.get(i).getDate_debut());

        TextView textViewDatefin = view.findViewById(R.id.textViewDateFin);
        textViewDatefin.setText(experiences.get(i).getDate_fin());

        TextView textViewFromations = view.findViewById(R.id.textViewSociete);
        textViewFromations.setText(experiences.get(i).getEntreprise());

        TextView textViewDescription = view.findViewById(R.id.textViewDescription);
        textViewDescription.setText(experiences.get(i).getDescription());

        return view;
    }
}
