package com.example.moncvapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ProjectsBaseAdapter extends BaseAdapter {
    private ArrayList<Project> projects;
    private Context context;

    public ProjectsBaseAdapter(ArrayList<Project> projects, Context context) {
        this.projects = projects;
        this.context = context;
    }


    @Override
    public int getCount() {
        return projects.size();
    }

    @Override
    public Object getItem(int i) {
        return projects.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null)
        {
            view = LayoutInflater.from(context).inflate(R.layout.item_projets, viewGroup, false);
        }

        TextView textViewDateProjects = view.findViewById(R.id.textNomProjet);
        textViewDateProjects.setText(projects.get(i).getNom());

        TextView textViewDateDescription = view.findViewById(R.id.textViewDescriptionProject);
        textViewDateDescription.setText(projects.get(i).getDescription());

        Button imageButton= view.findViewById(R.id.imageButtonProject);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(projects.get(i).getLien()));
                context.startActivity(intent);
            }
        });

        return view;
    }


}
